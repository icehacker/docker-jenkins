FROM centos:centos7

RUN yum install java -y

RUN yum install wget -y

RUN yum install git -y

RUN wget --no-check-certificate -O /etc/yum.repos.d/jenkins.repo https://pkg.jenkins.io/redhat-stable/jenkins.repo

RUN rpm --import https://pkg.jenkins.io/redhat-stable/jenkins.io.key

RUN sed -i 's/gpgcheck=1/gpgcheck=0/g' /etc/yum.repos.d/jenkins.repo

RUN yum install jenkins -y

WORKDIR /opt

RUN wget https://download.java.net/java/GA/jdk13/5b8a42f3905b406298b72d750b6919f6/33/GPL/openjdk-13_linux-x64_bin.tar.gz

RUN tar -xvf openjdk-13_linux-x64_bin.tar.gz

RUN echo "export JAVA_HOME=/opt/jdk-13/" >> /root/.bashrc

RUN echo "export PATH=$PATH:/opt/jdk-13/bin" >> /root/.bashrc

RUN source /root/.bashrc

RUN echo $JAVA_HOME

RUN java -version

WORKDIR /usr/local/src

RUN wget --no-check-certificate https://dlcdn.apache.org/maven/maven-3/3.5.4/binaries/apache-maven-3.5.4-bin.tar.gz

RUN tar -xf apache-maven-3.5.4-bin.tar.gz

RUN mv apache-maven-3.5.4/ apache-maven/

RUN echo "export MAVEN_HOME=/usr/local/src/apache-maven" >> /root/.bashrc

RUN echo "export PATH=${MAVEN_HOME}/bin:${PATH}" >> /root/.bashrc

RUN source /root/.bashrc

RUN mkdir -p /root/.jenkins/workspace/

WORKDIR /root/.jenkins/workspace/

EXPOSE 8080

CMD ["java", "-jar", "/usr/share/java/jenkins.war", "--webroot=/var/cache/jenkins/war", "--httpPort=8080"]




